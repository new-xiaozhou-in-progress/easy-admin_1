package com.mars.module.admin.mapper;

import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.admin.entity.SysMessage;
import com.mars.module.admin.entity.SysUserMessageStats;

/**
 * 消息Mapper接口
 *
 * @author mars
 * @date 2023-12-06
 */
public interface SysUserMessageStatsMapper extends BasePlusMapper<SysUserMessageStats> {

}
