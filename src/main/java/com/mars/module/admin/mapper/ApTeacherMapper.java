package com.mars.module.admin.mapper;

import com.mars.module.admin.entity.ApTeacher;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 教师Mapper接口
 *
 * @author mars
 * @date 2024-03-23
 */
public interface ApTeacherMapper extends BasePlusMapper<ApTeacher> {

}
