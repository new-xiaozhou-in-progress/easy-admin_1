package com.mars.module.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.*;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.mars.module.system.entity.BaseEntity;

/**
 * 教师对象 ap_teacher
 *
 * @author mars
 * @date 2024-03-23
 */

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "教师对象")
@Builder
@Accessors(chain = true)
@TableName("ap_teacher")
public class ApTeacher extends BaseEntity {

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;
    /**
     * 名称
     */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;
    /**
     * 头像
     */
    @Excel(name = "头像")
    @ApiModelProperty(value = "头像")
    private String headPortrait;
}
