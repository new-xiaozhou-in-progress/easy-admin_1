package com.mars.module.system.mapper;

import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.system.entity.SysUserRole;

/**
 * 用户关联角色
 *
 * @author 源码字节-程序员Mars
 */
public interface SysUserRoleMapper extends BasePlusMapper<SysUserRole> {


    /**
     * 删除用户角色关联关系
     *
     * @param userId 用户ID
     */
    void deleteByUserId(Long userId);
}
