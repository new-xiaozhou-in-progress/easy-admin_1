package com.mars.module.oss.strategy;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.mars.framework.exception.ServiceException;
import com.mars.module.admin.entity.SysOss;
import com.mars.module.admin.service.ISysOssService;
import com.mars.module.oss.common.enums.FileUploadTypeEnums;
import com.mars.module.oss.factory.StrategyFactory;
import com.mars.module.oss.manager.MinioManager;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * minio图片上传策略
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2024-02-29 09:21:50
 */
@Service
@ConditionalOnProperty(prefix = "easy.admin", name = "fileUploadType", havingValue = "minio")
public class MinioStrategy extends AbstractOssStrategy implements Strategy {

    @Resource
    private ISysOssService sysOssService;

    @Resource
    private MinioManager minioManager;

    @Override
    public String upload(MultipartFile file) throws Exception {
        return minioManager.uploadImage(file);
    }

    @Override
    public void afterPropertiesSet() {
        StrategyFactory.register(FileUploadTypeEnums.MINIO, this);
    }

    @Override
    public SysOss getOssConfig() {
        List<SysOss> list = sysOssService.list(FileUploadTypeEnums.MINIO.getType());
        if (CollectionUtils.isEmpty(list)) {
            throw new ServiceException("oss未配置");
        }
        return list.get(0);
    }
}
