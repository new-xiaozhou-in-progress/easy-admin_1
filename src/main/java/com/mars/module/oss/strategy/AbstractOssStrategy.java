package com.mars.module.oss.strategy;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.mars.framework.exception.ServiceException;
import com.mars.module.admin.entity.SysOss;
import com.mars.module.admin.service.ISysConfigService;
import com.mars.module.admin.service.ISysOssService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2024-02-29 10:26:17
 */

public abstract class AbstractOssStrategy {


    /**
     * 获取oss配置
     *
     * @return SysOss
     */
    public abstract SysOss getOssConfig();

}
