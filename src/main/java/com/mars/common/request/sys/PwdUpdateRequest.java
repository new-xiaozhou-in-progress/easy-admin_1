package com.mars.common.request.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 密码修改DTO
 *
 * @author 源码字节-程序员Mars
 */
@Data
@ApiModel(value = "")
public class PwdUpdateRequest {

    @NotEmpty
    @ApiModelProperty(value = "旧密码")
    private String oldPwd;

    @NotEmpty
    @ApiModelProperty(value = "新密码")
    private String newPwd;

    @NotEmpty
    @ApiModelProperty(value = "确认密码")
    private String confirmPwd;

}
